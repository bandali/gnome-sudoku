# Ales Nyakhaychyk <nab@mail.by>, 2003.
# Ihar Hrachyshka <ihar.hrachyshka@gmail.com>, 2011, 2013.
# Kasia Bondarava <kasia.bondarava@gmail.com>, 2012.
# Yuras Shumovich <shumovichy@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: gnome-games master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-sudoku/issues\n"
"POT-Creation-Date: 2022-11-01 22:24+0000\n"
"PO-Revision-Date: 2022-11-28 02:43+0300\n"
"Last-Translator: Launchpad translators\n"
"Language-Team: Belarusian <i18n-bel-gnome@googlegroups.com>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Poedit 3.2\n"
"X-Project-Style: gnome\n"

#: data/sudoku-window.ui:7
msgid "_New Puzzle"
msgstr "_Новая галаваломка"

#: data/sudoku-window.ui:11
msgid "_Clear Board"
msgstr "_Ачысціць дошку"

#: data/sudoku-window.ui:17 data/print-dialog.ui:38
msgid "_Print"
msgstr "_Друк"

#: data/sudoku-window.ui:20
msgid "Print _Current Puzzle…"
msgstr "Надрукаваць _бягучую галаваломку…"

#: data/sudoku-window.ui:24
msgid "Print _Multiple Puzzles…"
msgstr "Надрукаваць _некалькі галаваломак…"

#: data/sudoku-window.ui:32
msgid "High_lighter"
msgstr "Пад_светка"

#: data/sudoku-window.ui:36
msgid "_Warnings"
msgstr "_Папярэджанні"

#: data/sudoku-window.ui:42
msgid "_Help"
msgstr "_Даведка"

#: data/sudoku-window.ui:46
msgid "_About Sudoku"
msgstr "_Пра Судоку"

#: data/sudoku-window.ui:52 data/sudoku-window.ui:57
#: data/org.gnome.Sudoku.desktop.in:3 src/gnome-sudoku.vala:498
#: src/sudoku-window.vala:213
msgid "Sudoku"
msgstr "Судоку"

#: data/sudoku-window.ui:73
msgid "Undo your last action"
msgstr "Адрабіць апошняе дзеянне"

#: data/sudoku-window.ui:94
msgid "Redo your last action"
msgstr "Паўтарыць скасаванае дзеянне"

#: data/sudoku-window.ui:126
msgid "Pause"
msgstr "Прыпыніць"

#: data/sudoku-window.ui:147
msgid "Start playing the custom puzzle you have created"
msgstr "Пачаць уласную галаваломку, якую вы толькі што стварылі"

#: data/sudoku-window.ui:189
msgid "Go back to the current game"
msgstr "Вярнуцца да бягучай гульні"

#: data/sudoku-window.ui:233 data/print-dialog.ui:157
msgid "_Easy"
msgstr "_Просты"

#: data/sudoku-window.ui:241 data/print-dialog.ui:172
msgid "_Medium"
msgstr "_Сярэдні"

#: data/sudoku-window.ui:249 data/print-dialog.ui:188
msgid "_Hard"
msgstr "_Цяжкі"

#: data/sudoku-window.ui:257 data/print-dialog.ui:204
msgid "_Very Hard"
msgstr "_Вельмі цяжкі"

#: data/sudoku-window.ui:265
msgid "_Create your own puzzle"
msgstr "_Стварыць сваю галаваломку"

#: data/org.gnome.Sudoku.appdata.xml.in:7
msgid "GNOME Sudoku"
msgstr "GNOME Судоку"

#: data/org.gnome.Sudoku.appdata.xml.in:8 data/org.gnome.Sudoku.desktop.in:4
msgid "Test your logic skills in this number grid puzzle"
msgstr "Праверце сваё лагічнае мышленне на гэтай лічбавай галаваломцы"

#: data/org.gnome.Sudoku.appdata.xml.in:10
msgid ""
"Play the popular Japanese logic game. GNOME Sudoku is a must-install for "
"Sudoku lovers, with a simple, unobtrusive interface that makes playing "
"Sudoku fun for players of any skill level."
msgstr ""
"Гуляйце ў папулярную Японскую лагічную гульню. Раім GNOME Судоку ўсім "
"аматарам судоку, бо яго просты і ненадакучлівы інтэрфейс робіць гульню "
"вясёлай незалежна ад вашага майстэрства."

#: data/org.gnome.Sudoku.appdata.xml.in:15
msgid ""
"Each game is assigned a difficulty similar to those given by newspapers and "
"websites, so your game will be as easy or as difficult as you want it to be."
msgstr ""
"Кожная гульня мае ўзровень складанасці, падобны на той, які пазначаюць у "
"газетах і на сеціўных пляцоўках, таму гульня можа быць настолькі лёгкай ці "
"складанай, на колькі вы жадаеце."

#: data/org.gnome.Sudoku.appdata.xml.in:20
msgid ""
"If you like to play on paper, you can print games out. You can choose how "
"many games you want to print per page and what difficulty of games you want "
"to print: as a result, GNOME Sudoku can act a renewable Sudoku book for you."
msgstr ""
"Калі вам падабаецца гуляць на паперы, то галаваломкі можна надрукаваць. "
"Выберыце колькасць галаваломак на кожнай старонцы і іх узровень складанасці. "
"Такім чынам GNOME Судоку можа адыгрываць ролю бясконцай кнігі з судоку."

#: data/org.gnome.Sudoku.appdata.xml.in:30
msgid "A GNOME sudoku game preview"
msgstr "Папярэдні агляд гульні GNOME Судоку"

#: data/org.gnome.Sudoku.appdata.xml.in:75
msgid "The GNOME Project"
msgstr "Праект GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Sudoku.desktop.in:6
msgid "magic;square;"
msgstr "магічны;квадрат;"

#: data/org.gnome.Sudoku.gschema.xml:11
msgid "Difficulty level of sudokus to be printed"
msgstr "Узровень складанасці судоку для друку"

#: data/org.gnome.Sudoku.gschema.xml:12
msgid ""
"Set the difficulty level of the sudokus you want to print. Possible values "
"are: \"easy\", \"medium\", \"hard\", \"very_hard\""
msgstr ""
"Задайце ўзровень складанасці судоку, якія вы хочаце надрукаваць. Магчымыя "
"значэнні: \"easy\" (лёгка), \"medium\" (сярэдне), \"hard\" (цяжка), "
"\"very_hard\" (вельмі цяжка)"

#: data/org.gnome.Sudoku.gschema.xml:17
msgid "Number of Sudokus to print"
msgstr "Колькасць судоку для друку"

#: data/org.gnome.Sudoku.gschema.xml:18
msgid "Set the number of sudokus you want to print"
msgstr "Задайце колькасць судоку, якія вы хочаце надрукаваць"

#: data/org.gnome.Sudoku.gschema.xml:23
msgid "Number of Sudokus to print per page"
msgstr "Колькасць судоку на старонцы для друку"

#: data/org.gnome.Sudoku.gschema.xml:24
msgid "Set the number of sudokus you want to print per page"
msgstr "Задайце якую колькасць судоку вы хочаце надрукаваць на старонцы"

#: data/org.gnome.Sudoku.gschema.xml:28
msgid "Warn about unfillable squares and duplicate numbers"
msgstr "Папярэджваць пра паўторныя лічбы і квадраты, якія немагчыма запоўніць"

#: data/org.gnome.Sudoku.gschema.xml:29
msgid ""
"Displays a big red X in a square if it cannot possibly be filled by any "
"number and duplicate numbers are highlighted in red"
msgstr ""
"Паказвае вялікі чырвоны крыжык у квадраце, калі яго нельга запоўніць ніякай "
"лічбай, паўторныя лічбы падсвечваюцца чырвоным"

#: data/org.gnome.Sudoku.gschema.xml:33
msgid "Highlight row, column and square that contain the selected cell"
msgstr "Падсвяціць радок, слупок і квадрат, якія змяшчаюць выбраную клетку"

#: data/org.gnome.Sudoku.gschema.xml:37
msgid "Width of the window in pixels"
msgstr "Шырыня акна ў пікселях"

#: data/org.gnome.Sudoku.gschema.xml:41
msgid "Height of the window in pixels"
msgstr "Вышыня акна ў пікселях"

#: data/org.gnome.Sudoku.gschema.xml:45
msgid "true if the window is maximized"
msgstr "уключана, калі акно разгорнута"

#: data/org.gnome.Sudoku.gschema.xml:49
msgid "Initialize the earmarks with the possible values for each cell"
msgstr "Паказваць магчымыя значэнні для кожнай клеткі"

#: data/print-dialog.ui:18
msgid "Print Multiple Puzzles"
msgstr "Надрукаваць некалькі галаваломак"

#: data/print-dialog.ui:27
msgid "_Cancel"
msgstr "_Скасаваць"

#: data/print-dialog.ui:65
msgid "_Number of puzzles"
msgstr "_Колькасць галаваломак"

#: data/print-dialog.ui:101
msgid "_Number of puzzles per page"
msgstr "_Колькасць галаваломак на старонцы"

#: data/print-dialog.ui:138
msgid "Difficulty"
msgstr "Складанасць"

#: lib/sudoku-board.vala:644
msgid "Unknown Difficulty"
msgstr "Невядомая складанасць"

#: lib/sudoku-board.vala:646
msgid "Easy Difficulty"
msgstr "Проста"

#: lib/sudoku-board.vala:648
msgid "Medium Difficulty"
msgstr "Сярэдне"

#: lib/sudoku-board.vala:650
msgid "Hard Difficulty"
msgstr "Цяжка"

#: lib/sudoku-board.vala:652
msgid "Very Hard Difficulty"
msgstr "Вельмі цяжка"

#: lib/sudoku-board.vala:654
msgid "Custom Puzzle"
msgstr "Свая галаваломка"

#. Help string for command line --version flag
#: src/gnome-sudoku.vala:72
msgid "Show release version"
msgstr "Паказаць версію выдання"

#. Help string for command line --show-possible flag
#: src/gnome-sudoku.vala:76
msgid "Show the possible values for each cell"
msgstr "Паказаць магчымыя значэнні для кожнай клеткі"

#. Error dialog shown when starting a custom game that is not valid.
#: src/gnome-sudoku.vala:231
msgid "The puzzle you have entered is not a valid Sudoku."
msgstr "Уведзеная галаваломка не з'яўляецца правільным судоку."

#: src/gnome-sudoku.vala:231
msgid "Please enter a valid puzzle."
msgstr "Увядзіце правільную галаваломку."

#. Warning dialog shown when starting a custom game that has multiple solutions.
#: src/gnome-sudoku.vala:240
msgid "The puzzle you have entered has multiple solutions."
msgstr "Уведзеная галаваломка мае некалькі рашэнняў."

#: src/gnome-sudoku.vala:240
msgid "Valid Sudoku puzzles have exactly one solution."
msgstr "Правільныя судоку маюць адзінае рашэнне."

#: src/gnome-sudoku.vala:242
msgid "_Back"
msgstr "_Назад"

#: src/gnome-sudoku.vala:243
msgid "Play _Anyway"
msgstr "Гуляць усё роўна"

#: src/gnome-sudoku.vala:286
#, c-format
msgid "Well done, you completed the puzzle in %d minute!"
msgid_plural "Well done, you completed the puzzle in %d minutes!"
msgstr[0] "Выдатна, вы рашылі галаваломку за %d хвіліну!"
msgstr[1] "Выдатна, вы рашылі галаваломку за %d хвіліны!"
msgstr[2] "Выдатна, вы рашылі галаваломку за %d хвілін!"

#: src/gnome-sudoku.vala:290
msgid "_Quit"
msgstr "_Выйсці"

#: src/gnome-sudoku.vala:291
msgid "Play _Again"
msgstr "Гуляць _зноў"

#: src/gnome-sudoku.vala:400
msgid "Reset the board to its original state?"
msgstr "Сапраўды вярнуць дашку ў першапачатковы стан?"

#. Appears on the About dialog. %s is the version of the QQwing puzzle generator in use.
#: src/gnome-sudoku.vala:495
#, c-format
msgid ""
"The popular Japanese logic puzzle\n"
"\n"
"Puzzles generated by QQwing %s"
msgstr ""
"Папулярная японская галаваломка\n"
"\n"
"Галаваломкі згенераваны QQwing %s"

#: src/gnome-sudoku.vala:506
msgid "translator-credits"
msgstr ""
"Ales Nyakhaychyk <nab@mail.by>\n"
"Ihar Hrachyshka <ihar.hrachyshka@gmail.com>\n"
"Юрась Шумовіч <shumovichy@gmail.com>"

#: src/number-picker.vala:85
msgid "Clear"
msgstr "Ачысціць"

#. Error message if printing fails
#: src/sudoku-printer.vala:47
msgid "Error printing file:"
msgstr "Памылка друкавання файла:"

#. Text on overlay when game is paused
#: src/sudoku-view.vala:806
msgid "Paused"
msgstr "Паўза"

#: src/sudoku-window.vala:154
msgid "Select Difficulty"
msgstr "Выберыце складанасць"

#: src/sudoku-window.vala:215
msgid "Create Puzzle"
msgstr "Стварыць галаваломку"
